import { Application, Request, Response } from 'express';
import LogRoutes from '../../modules/logs/routes';

class Routes {

    private router: LogRoutes;

    constructor(app: Application){
        this.router = new LogRoutes();
        this.getRoutes(app);
    }

    getRoutes(app: Application): void {
        app.route('/all')
        .get(this.router.all);
        
        app.route('/show/:log')
        .get(this.router.show);
    }
}

export default Routes;