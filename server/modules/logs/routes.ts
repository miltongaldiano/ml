import { Application, Request, Response } from 'express';

export default class LogRoutes {

    constructor(){
    }

    all(req: Request, res: Response) {

        let logs: Object[] = [];

        let fs = require('fs');
        let path = require('path');
        let readStream = fs.createReadStream(path.join(__dirname, '../../log') + '/games.log', 'utf8');
        
        fs.readFile(readStream.path , function(err,data)
        {
            if(err)
                console.log(err)
            else

                var lines = data.toString().split("\n");
                let games = 0;
                let total_kills = 0;
                let player = [];
                let player_kills: Object;
                let kills_p = []

                for(var l in lines) {
                    
                    if(lines[l].indexOf('InitGame') > 0 || lines[l].indexOf('Kill') > 0) {
                        
                        if(lines[l].indexOf('Kill') > 0) {

                            total_kills++;

                            if(lines[l].indexOf('<world>') > 0){

                                let p  = lines[l].match("killed (.*) by")[1];
                                
                                if(player.indexOf(p) == -1){
                                    kills_p[p] = (kills_p[p] > 0 ? kills_p[p]-- : 0);
                                    player_kills[p] = kills_p[p];
                                }


                            } else {

                                let t = lines[l].split(':')[3];
                                
                                var p  = t.match(" (.*) killed")[1];
                                var p2  = lines[l].match("killed (.*) by")[1];

                                if(player.indexOf(p) == -1){

                                    player.push(p);
                                    player_kills[p] = kills_p[p]++;
                                }

                                if(player.indexOf(p2) == -1){

                                    player.push(p2);

                                }                                

                            }

                            logs['game_'+games] = { total_kills : total_kills };
                            logs['game_'+games].players = player;

                            logs['game_'+games].kills = player_kills;                            

                        } else {
                            games++;
                            total_kills = 0;
                            player = [];
                            player_kills = {};
                            kills_p[p] = 0;
                        }
                        
                    }
                }
                
                console.log(logs);
        }); 

    };

    show(req: Request, res: Response) {
        res.send('show');
    }

}