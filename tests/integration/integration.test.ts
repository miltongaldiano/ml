import { app, request, expect } from './config/helpers';

describe('Testes de Integração', () => {

    describe('Método all', () => {
        describe('GET /', () => {
            it('Deve mostrar todos os registros de log', done => {

                request(app)
                    .get('/all')
                    .end((error, res) => {
                        expect(res.status).to.equal(200);
                    });

            });
        });
    });

    describe('Método show', () => {
        it('Deve mostrar apenas um registro de log', () => {

                request(app)
                    .get('/')
                    .end((error, res) => {
                        expect(res.status).to.equal(200);
                    });

        });
    });
    
});